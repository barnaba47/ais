#!/bin/sh

lang="en_US.UTF-8 UTF-8"
#lang="pl_PL.UTF-8 UTF-8"

lang2=$(echo "$lang" | cut -d ' ' -f 1)
keymap=$(echo "$lang" | cut -c 4-5 )
keymap=$(echo "$keymap" | awk '{print tolower($0)}')


user=$(whoami)
tmp="/tmp"
nmtui
clear
echo "Enable NTP"
echo " "
sudo timedatectl set-ntp true
 

function main()
{
	sudo rm -f /etc/fstab
	sudo mv /etc/fstab.b /etc/fstab
	sudo snapper -c root create-config /
	sudo snapper -c home create-config /home

	# Configuring locales
	sudo mv /etc/locale.gen /etc/locale.gen.bak
	echo $lang | sudo tee -a /etc/locale.gen
	sudo mv /etc/locale.conf /etc/locale.conf.bak
	echo LANG='"'$lang2'"' | sudo tee -a /etc/locale.conf
	sudo locale-gen

	# Installing packages
	sudo pacman -S archlinux-keyring hyprland thunar pipewire pipewire-alsa pipewire-pulse pipewire-jack wireplumber \
		pavucontrol papirus-icon-theme file-roller ttf-hack cantarell-fonts kitty firefox hyprpaper ranger \
		gsimplecal lxsession-gtk3 dunst noto-fonts-emoji bluez bluez-utils blueman gamescope hypridle grim \
		slurp xdg-utils xdg-user-dirs xdg-desktop-portal-hyprland vim wl-clipboard imv acpid wofi \
		otf-font-awesome libadwaita ttf-roboto cpupower brightnessctl xorg-xsetroot net-tools libnotify \
		brightnessctl playerctl usbutils waybar gvfs gvfs-mtp hyprlock gnome-text-editor ttf-hack-nerd adw-gtk-theme \
		nwg-dock-hyprland nwg-look
	


	# Enabling services
	systemctl --user enable --now pipewire.socket
	systemctl --user enable --now pipewire-pulse.socket
	systemctl --user enable --now wireplumber.service

	sudo systemctl enable --now bluetooth.service

	sudo systemctl enable --now acpid.service

	# Creating user's home subdirectories
	rm -rf ~/*
	export LANG=$lang2
	xdg-user-dirs-update --force

	mkdir -p $(xdg-user-dir PICTURES)/Screenshots

	# Setting Thunar as default file manager
	xdg-mime default thunar.desktop inode/directory
}

function dotfiles()
{
	cd $tmp && git clone https://gitlab.com/barnaba47/ais && cd ais && tar xf hyprland.tar.xz && cp -r $tmp/ais/home/. /home/$user
	
	gsettings set org.gnome.desktop.interface gtk-theme "adw-gtk3-dark"
	gsettings set org.gnome.desktop.interface color-scheme 'prefer-dark'
	gsettings set org.gnome.desktop.interface icon-theme 'Papirus-Dark'
	gsettings set org.gnome.desktop.interface cursor-theme 'DMZ-White'
	gsettings set org.gnome.desktop.interface cursor-size 24
	gsettings set org.gnome.desktop.interface font-name 'Roboto 11'
	gsettings set org.gnome.desktop.interface monospace-font-name 'Hack 11'
	gsettings set org.gnome.desktop.interface font-hinting 'slight'
	gsettings set org.gnome.desktop.interface font-antialiasing 'rgba'
}

function aur()
{
	cd $tmp
	git clone https://aur.archlinux.org/adw-gtk3.git
	cd adw-gtk3
	makepkg -si

	cd $tmp
	git clone https://aur.archlinux.org/adw-gtk-theme.git
	cd adw-gtk-theme
	makepkg -si

	cd /home/$user
}


function amd()
{
	sudo pacman -S xf86-video-amdgpu mesa lib32-mesa vulkan-radeon lib32-vulkan-radeon vulkan-icd-loader lib32-vulkan-icd-loader 
}


function nvidia() 
{
    # Not tested!
	#sudo pacman -S nvidia nvidia-utils lib32-nvidia-utils nvidia-settings vulkan-icd-loader lib32-vulkan-icd-loader
	echo " "
}


function intel()
{
	sudo pacman -S xf86-video-intel mesa lib32-mesa vulkan-intel lib32-vulkan-intel vulkan-icd-loader lib32-vulkan-icd-loader
}


function vm()
{
	echo " "
}


function gpu_drv_pkgs()
{
	if [ $gpu_vendor == "amd" ]; 
		then
		amd 

	elif [ $gpu_vendor == "nvidia" ];
		then	
		nvidia	
	
	elif [ $gpu_vendor == "intel" ];
		then	
		intel
	
	elif [ $gpu_vendor == "vm" ];
		then	
		vm	

	else
		echo "Vendor not supported!"
		echo " "
		echo "Insert GPU vendor (amd, nvidia, intel, vm)"
		read gpu_vendor
		gpu_drv_pkgs
	fi
}


main

echo " "
echo "Insert GPU vendor (amd, nvidia, intel, vm)"
read gpu_vendor
echo " "
gpu_drv_pkgs

#aur

dotfiles


sed -i 's/PS1=/#PS1=/g' /home/$user/.bashrc
echo "PS1='(\[\033[1;94m\]\u@\h\[\033[1;34m\] \W\[\033[0m\])\$ '" >> /home/$user/.bashrc
echo 'export WLR_NO_HARDWARE_CURSORS=0' >> /home/$user/.bashrc
echo 'export EDITOR="vim"' >> /home/$user/.bashrc
echo 'export VISUAL="vim"' >> /home/$user/.bashrc
echo 'alias ll="ls -lah"' >> /home/$user/.bashrc

echo 'export PATH="$PATH:/home/'$user'/.scripts"' >> /home/$user/.bash_profile
echo 'export WLR_NO_HARDWARE_CURSORS=1' >> /home/$user/.bash_profile
echo 'export MOZ_ENABLE_WAYLAND=1' >> /home/$user/.bash_profile
echo "Hyprland &" >> /home/$user/.bash_profile

echo " "
echo " "
echo "Configuration finished!"
echo " "
echo " "

sleep 3

source /home/$user/.bash_profile