#!/bin/sh

#rm -r /etc/pacman.d/gnupg/
#killall gpg-agent
#pacman-key --init
#pacman-key --populate

clear
echo " "
echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo "++++++++++++++++++++++++++++++++| Arch Linux installation |++++++++++++++++++++++++++++++++++"
echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo " "
echo "              To check your disk's name stop this script (ctrl+c), then enter:"
echo " "
echo "                                          lsblk "
echo " "
echo "                To erase disk before installation run the following command:"
echo " "
echo "                     dd if=/dev/zero of=/dev/sdx bs=4M status=progress"
echo " "
echo "                                    or for nvme disk:"
echo " "
echo "                               nvme format /dev/nvme0nx -s1"
echo " "
echo " "
echo "Insert disk's name (e.g. sdx, nvme0nx)"
read disk
if [[ $disk == sd? ]] || [[ $disk == vd? ]];
then
    x1="1"
    x2="2"
    x3="3"
else
    x1="p1"
    x2="p2"
    x3="p3"
fi

disk1="$disk$x1"
disk2="$disk$x2"
disk3="$disk$x3"

clear

echo " "
echo " "
echo " !WARNING! !WARNING! !WARNING! !WARNING! !WARNING! !WARNING! !WARNING! !WARNING! !WARNING!"
echo " "
echo "                        Partition disk using the following scheme:"
echo " "
echo "-----------------------------------------------------------------------------------------"
echo "                                  GPT partition table "
echo "                         /dev/sdx1 = EFI filesystem    [boot]"
echo "                         /dev/sdx2 = Linux filesystem  [boot]"
echo "                         /dev/sdx3 = Linux filesystem  [system]"
echo "-----------------------------------------------------------------------------------------"
echo " "
echo " "
read -p "Press ENTER to begin installation"
cfdisk /dev/$disk
modprobe dm-crypt
modprobe dm-mod

echo " "
echo "Do you want to encrypt the root partition? (y/n)"
read encrypt

if [[ $encrypt == "y" || $encrypt == "Y" ]]; then
    cryptsetup luksFormat -v -s 512 -h sha512 /dev/$disk3
    echo " "
    echo "Open encrypted partition:"
    cryptsetup luksOpen /dev/$disk3 luks
    mkfs.btrfs -L ArchLinux /dev/mapper/luks
    mount -o noatime,discard,ssd,defaults /dev/mapper/luks /mnt
else
    mkfs.btrfs -L ArchLinux /dev/$disk3
    mount -o noatime,discard,ssd,defaults /dev/$disk3 /mnt
fi

btrfs subvolume create /mnt/@root
btrfs subvolume create /mnt/@home
btrfs subvolume create /mnt/@snapshots
umount /mnt

if [[ $encrypt == "y" || $encrypt == "Y" ]]; then
    mount -o subvol=@root /dev/mapper/luks /mnt
    mkdir /mnt/{home,var,.snapshots}
    mount -o subvol=@home /dev/mapper/luks /mnt/home
    mount -o subvol=@snapshots /dev/mapper/luks /mnt/.snapshots
else
    mount -o subvol=@root /dev/$disk3 /mnt
    mkdir /mnt/{home,var,.snapshots}
    mount -o subvol=@home /dev/$disk3 /mnt/home
    mount -o subvol=@snapshots /dev/$disk3 /mnt/.snapshots
fi

mkfs.vfat -F32 /dev/$disk1
mkfs.ext4 /dev/$disk2

function create_swap()
{
    if [[ $swap == y ]] || [[ $swap == Y ]];
    then
        echo " "
        echo "Insert SWAP size    (e.g. 4k/m/g/e/p)"
        read swap_size
        mkdir -p /mnt/swap
        btrfs filesystem mkswapfile --size $swap_size /mnt/swap/swapfile
        swapon /mnt/swap/swapfile

    elif [[ $swap == n ]] || [[ $swap == N ]];
    then
        echo " "  # nothing to do!
    else
        echo " "
        echo "Invalid option, try again!"
        echo " "
        echo "Create SWAP?    (y/n)"
        read swap
        create_swap   
    fi
}

echo " "
echo "Create SWAP?  (y/n)"
read swap
create_swap

mkdir -p /mnt/boot
mount /dev/$disk2 /mnt/boot
mkdir /mnt/boot/efi
mount /dev/$disk1 /mnt/boot/efi
clear
pacstrap /mnt linux linux-firmware base base-devel btrfs-progs snapper
genfstab -U /mnt >> /mnt/etc/fstab
mv /mnt/etc/fstab /mnt/etc/fstab.b
umount /mnt/.snapshots
rm -d /mnt/.snapshots
genfstab -U /mnt >> /mnt/etc/fstab
arch-chroot /mnt ln -sf /usr/share/zoneinfo/Europe/Warsaw /etc/localtime
arch-chroot /mnt hwclock --systohc --utc
sed -i '/en_US.UTF-8 UTF-8/s/^#//g' /mnt/etc/locale.gen
arch-chroot /mnt locale-gen
echo "LANG=en_US.UTF-8" > /mnt/etc/locale.conf
echo " "
echo "Set hostname:"
read host
echo "$host" > /mnt/etc/hostname
echo " "
echo "Set ROOT password:"
arch-chroot /mnt passwd
echo " "
echo "Set username:"
read user
arch-chroot /mnt useradd -m -G wheel -s /bin/bash $user
echo " "
echo "Set USER password:"
arch-chroot /mnt passwd $user
echo " "
sed -i '/%wheel ALL=(ALL:ALL) ALL/s/^#//g' /mnt/etc/sudoers
#sed -i '33s/#//' /mnt/etc/pacman.conf
#sed -i '93s/#//' /mnt/etc/pacman.conf
#sed -i '94s/#//' /mnt/etc/pacman.conf
sed -i '/#Color/s/^#//' /mnt/etc/pacman.conf
sed -i '/#\[multilib\]/s/^#//' /mnt/etc/pacman.conf
sed -i '/\[multilib\]/{n;s/^#//}' /mnt/etc/pacman.conf
arch-chroot /mnt pacman -Sy grub efibootmgr lvm2 os-prober

if [[ $encrypt == "y" || $encrypt == "Y" ]]; then
    sed -i '/^GRUB_CMDLINE_LINUX/c\GRUB_CMDLINE_LINUX="cryptdevice=\/dev\/'$disk3':ArchLinux:allow-discards"' /mnt/etc/default/grub
    sed -i '/^HOOKS=/s/filesystems/encrypt lvm2 filesystems/' /mnt/etc/mkinitcpio.conf
fi

sed -i '/^GRUB_TIMEOUT/c\GRUB_TIMEOUT=0' /mnt/etc/default/grub
arch-chroot /mnt mkinitcpio -p linux
arch-chroot /mnt grub-install --boot-directory=/boot --efi-directory=/boot/efi /dev/$disk2 --bootloader-id=ArchLinux
arch-chroot /mnt grub-mkconfig -o /boot/grub/grub.cfg
arch-chroot /mnt grub-mkconfig -o /boot/efi/EFI/ArchLinux/grub.cfg
arch-chroot /mnt pacman -S networkmanager modemmanager usb_modeswitch git
arch-chroot /mnt systemctl enable systemd-networkd
arch-chroot /mnt systemctl enable systemd-resolved
arch-chroot /mnt systemctl enable NetworkManager
arch-chroot /mnt systemctl enable ModemManager
mv config.sh /mnt/home/$user
chmod 777 /mnt/home/$user/config.sh
echo " "
echo "Installation finished!"
echo " "
read -p "Press ENTER to reboot"
umount -R /mnt
reboot
