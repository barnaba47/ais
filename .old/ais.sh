#!/bin/sh

#rm -r /etc/pacman.d/gnupg/
#killall gpg-agent
#pacman-key --init
#pacman-key --populate

clear
echo " "
echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo "++++++++++++++++++++++++++++++++| Arch Linux installation |++++++++++++++++++++++++++++++++++"
echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo " "
echo "              To check your disk's name stop this script (ctrl+c), then enter:"
echo " "
echo "                                          lsblk "
echo " "
echo "                To erase disk before installation run the following command:"
echo " "
echo "                     dd if=/dev/zero of=/dev/sdx bs=4M status=progress"
echo " "
echo "                                    or for nvme disk:"
echo " "
echo "                               nvme format /dev/nvme0nx -s1"
echo " "
echo " "
echo "Insert disk's name (e.g. sdx, nvme0nx)"
read disk
if [ $disk == "sda" ] || [ $disk == "vda" ];
then
x1="1"
x2="2"
x3="3"
x4="4"
else
x1="p1"
x2="p2"
x3="p3"
x4="p4"
fi
disk1="$disk$x1"
disk2="$disk$x2"
disk3="$disk$x3"
disk4="$disk$x4"
clear
echo " "
echo " "
echo " !WARNING! !WARNING! !WARNING! !WARNING! !WARNING! !WARNING! !WARNING! !WARNING! !WARNING!"
echo " "
echo "                        Partition disk using the following sheme:"
echo " "
echo "-----------------------------------------------------------------------------------------"
echo "                                  GPT partition table "
echo "                         /dev/sdx1 = EFI filesystem    [boot]"
echo "                         /dev/sdx2 = Linux filesystem  [boot]"
echo "                         /dev/sdx3 = Linux swap        [swap]"
echo "                         /dev/sdx4 = Linux filesystem  [system]"
echo "-----------------------------------------------------------------------------------------"
echo " "
echo " "
read -p "Press ENTER to begin installation"
cfdisk /dev/$disk
modprobe dm-crypt
modprobe dm-mod
cryptsetup luksFormat -v -s 512 -h sha512 /dev/$disk4
echo " "
echo "Open encrypted partition:"
cryptsetup luksOpen /dev/$disk4 luks
mkfs.btrfs -L ArchLinux /dev/mapper/luks
mount -o noatime,discard,ssd,defaults /dev/mapper/luks /mnt
btrfs subvolume create /mnt/@root
btrfs subvolume create /mnt/@home
btrfs subvolume create /mnt/@snapshots
umount /mnt
mount -o subvol=@root /dev/mapper/luks /mnt
mkdir /mnt/{home,var,swap,.snapshots}
mount -o subvol=@home /dev/mapper/luks /mnt/home
mount -o subvol=@snapshots /dev/mapper/luks /mnt/.snapshots
mkfs.vfat -F32 /dev/$disk1
mkfs.ext4 /dev/$disk2
mkswap /dev/$disk3
swapon /dev/$disk3
mkdir -p /mnt/boot
mount /dev/$disk2 /mnt/boot
mkdir /mnt/boot/efi
mount /dev/$disk1 /mnt/boot/efi
clear
pacstrap /mnt linux linux-firmware base base-devel btrfs-progs snapper
genfstab -U /mnt >> /mnt/etc/fstab
mv /mnt/etc/fstab /mnt/etc/fstab.b
umount /mnt/.snapshots
rm -d /mnt/.snapshots
genfstab -U /mnt >> /mnt/etc/fstab
arch-chroot /mnt ln -sf /usr/share/zoneinfo/Europe/Warsaw /etc/localtime
arch-chroot /mnt hwclock --systohc --utc
#sed -i '177s/#//' /mnt/etc/locale.gen
#sed -i '390s/#//' /mnt/etc/locale.gen
sed -i '/en_US.UTF-8 UTF-8/s/^#//g' /mnt/etc/locale.gen
arch-chroot /mnt locale-gen
echo "LANG=en_US.UTF-8" > /mnt/etc/locale.conf
echo " "
echo "Set hostname:"
read host
echo "$host" > /mnt/etc/hostname
echo " "
echo "Set ROOT password:"
arch-chroot /mnt passwd
echo " "
echo "Set username:"
read user
arch-chroot /mnt useradd -m -G wheel -s /bin/bash $user
echo " "
echo "Set USER password:"
arch-chroot /mnt passwd $user
echo " "
#sed -i '85s/# //' /mnt/etc/sudoers
sed -i '/%wheel ALL=(ALL:ALL) ALL/s/^#//g' /mnt/etc/sudoers
sed -i '33s/#//' /mnt/etc/pacman.conf
sed -i '93s/#//' /mnt/etc/pacman.conf
sed -i '94s/#//' /mnt/etc/pacman.conf
arch-chroot /mnt pacman -Sy grub efibootmgr lvm2 os-prober
sed -i '7s/=""/="cryptdevice=\/dev\/'$disk4':ArchLinux:allow-discards"/' /mnt/etc/default/grub
sed -i '4s/=5/=0/' /mnt/etc/default/grub
sed -i '52s/filesystems/encrypt lvm2 filesystems/' /mnt/etc/mkinitcpio.conf
arch-chroot /mnt mkinitcpio -p linux
arch-chroot /mnt grub-install --boot-directory=/boot --efi-directory=/boot/efi /dev/$disk2 --bootloader-id=ArchLinux
arch-chroot /mnt grub-mkconfig -o /boot/grub/grub.cfg
arch-chroot /mnt grub-mkconfig -o /boot/efi/EFI/ArchLinux/grub.cfg
arch-chroot /mnt pacman -S networkmanager modemmanager usb_modeswitch git #xdg-user-dir
arch-chroot /mnt systemctl enable systemd-networkd
arch-chroot /mnt systemctl enable systemd-resolved
arch-chroot /mnt systemctl enable NetworkManager
arch-chroot /mnt systemctl enable ModemManager
mv config.sh config-gnome.sh config_h.sh /mnt/home/$user
chmod 777 /mnt/home/$user/config.sh /mnt/home/$user/config-gnome.sh
echo " "
echo "Installation finished!"
echo " "
read -p "Press ENTER to reboot"
umount -R /mnt
reboot
