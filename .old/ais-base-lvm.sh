#!/bin/sh
echo " "
echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo "++++++++++++++++++++++++++++++++| Arch Linux installation |++++++++++++++++++++++++++++++++++"
echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo " "
echo "              To check your disk's name stop this script (ctrl+c), then enter:"
echo " "
echo "                                          lsblk "
echo " "
echo "                To erase disk before installation run the following command:"
echo " "
echo "                     dd if=/dev/zero of=/dev/sdx bs=4M status=progress"
echo " "
echo " "
echo "Insert disk's name (e.g. sdx, nvme0nx)"
read disk
if [ $disk == "sda" ] || [ $disk == "vda" ];
then
x1="1"
x2="2"
x3="3"
else
x1="p1"
x2="p2"
x3="p3"
fi
disk1="$disk$x1"
disk2="$disk$x2"
disk3="$disk$x3"
clear
echo " "
echo " "
echo " !WARNING! !WARNING! !WARNING! !WARNING! !WARNING! !WARNING! !WARNING! !WARNING! !WARNING!"
echo " "
echo "                        Partition disk using the following sheme:"
echo " "
echo " -----------------------------------------------------------------------------------------"
echo "          /dev/sdx1 = system + swap | /dev/sdx2 = boot (ext) | /dev/sdx3 = efi"
echo " -----------------------------------------------------------------------------------------"
echo " "
echo " "
read -p "Press ENTER to begin installation"
cfdisk /dev/$disk
modprobe dm-crypt
modprobe dm-mod
cryptsetup luksFormat -v -s 512 -h sha512 /dev/$disk1
echo " "
echo "Open encrypted partition:"
cryptsetup open /dev/$disk1 lvm
pvcreate /dev/mapper/lvm
vgcreate vg0 /dev/mapper/lvm
lvcreate -L 2G vg0 -n swap
lvcreate -l 90%FREE vg0 -n root
mkfs.ext4 /dev/mapper/vg0-root
mkfs.ext4 /dev/$disk2
mkfs.vfat -F32 /dev/$disk3
mkswap /dev/mapper/vg0-swap
mount /dev/mapper/vg0-root /mnt
mkdir -p /mnt/boot
mount /dev/$disk2 /mnt/boot
mkdir /mnt/boot/efi
mount /dev/$disk3 /mnt/boot/efi
swapon /dev/mapper/vg0-swap
clear
pacstrap /mnt linux linux-firmware base base-devel nano
genfstab -U /mnt >> /mnt/etc/fstab
arch-chroot /mnt ln -sf /usr/share/zoneinfo/Europe/Warsaw /etc/localtime
arch-chroot /mnt hwclock --systohc --utc
sed -i '177s/#//' /mnt/etc/locale.gen
sed -i '390s/#//' /mnt/etc/locale.gen
arch-chroot /mnt locale-gen
echo "LANG=pl_PL.UTF-8" > /mnt/etc/locale.conf
echo " "
echo "Set hostname:"
read host
echo "$host" > /mnt/etc/hostname
echo " "
echo "Set ROOT password:"
arch-chroot /mnt passwd
echo " "
echo "Set username:"
read user
arch-chroot /mnt useradd -m -G wheel -s /bin/bash $user
echo " "
echo "Set USER password:"
arch-chroot /mnt passwd $user
echo " "
sed -i '82s/# //' /mnt/etc/sudoers
sed -i '33s/#//' /mnt/etc/pacman.conf
sed -i '93s/#//' /mnt/etc/pacman.conf
sed -i '94s/#//' /mnt/etc/pacman.conf
arch-chroot /mnt pacman -Sy grub efibootmgr lvm2 os-prober
sed -i '7s/=""/="cryptdevice=\/dev\/'$disk1':ArchLinux:allow-discards"/' /mnt/etc/default/grub
sed -i '4s/=5/=0/' /mnt/etc/default/grub
sed -i '52s/filesystems/encrypt lvm2 filesystems/' /mnt/etc/mkinitcpio.conf
arch-chroot /mnt mkinitcpio -p linux
arch-chroot /mnt grub-install --boot-directory=/boot --efi-directory=/boot/efi /dev/$disk2 --bootloader-id=ArchLinux
arch-chroot /mnt grub-mkconfig -o /boot/grub/grub.cfg
arch-chroot /mnt grub-mkconfig -o /boot/efi/EFI/ArchLinux/grub.cfg
arch-chroot /mnt pacman -S networkmanager modemmanager usb_modeswitch xdg-user-dirs git
arch-chroot /mnt systemctl enable systemd-networkd
arch-chroot /mnt systemctl enable systemd-resolved
arch-chroot /mnt systemctl enable NetworkManager
arch-chroot /mnt systemctl enable ModemManager
#mv config.sh /mnt/home/$user
#chmod 777 /mnt/home/$user/config.sh
echo " "
echo "Installation finished!"
echo " "
read -p "Press ENTER to reboot"
umount -R /mnt
reboot
