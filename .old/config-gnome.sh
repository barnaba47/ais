#!/bin/sh

lang="pl_PL.UTF-8 UTF-8"

lang2=$(echo "$lang" | cut -d ' ' -f 1)
keymap=$(echo "$lang" | cut -c 4-5 )
keymap=$(echo "$keymap" | awk '{print tolower($0)}')

user=$(whoami)
tmp="/tmp"
nmtui
clear
echo "Enable NTP"
echo " "
sudo timedatectl set-ntp true


function main()
{
	sudo rm -f /etc/fstab
	sudo mv /etc/fstab.b /etc/fstab
	sudo snapper -c root create-config /	

	cd $tmp && git clone https://aur.archlinux.org/yay.git && cd yay && makepkg -si

	yay -S gnome-shell gdm xdg-utils xdg-user-dirs gnome-console nautilus gnome-backgrounds gnome-tweaks gnome-control-center gnome-calculator firefox file-roller android-udev android-tools gthumb bash-completion gvfs-mtp reflector papirus-icon-theme papirus-folders-git noto-fonts-emoji ttf-hack adw-gtk3-git extension-manager-git

	sudo mv /etc/locale.gen /etc/locale.gen.bak
	echo $lang | sudo tee -a /etc/locale.gen
	sudo mv /etc/locale.conf /etc/locale.conf.bak
	echo LANG='"'$lang2'"' | sudo tee -a /etc/locale.conf
	sudo locale-gen
	sudo localectl set-keymap --no-convert $keymap
	sudo localectl set-x11-keymap --no-convert $keymap

	papirus-folders -C adwaita --theme Papirus-Dark
}


function amd()
{
	yay -S xf86-video-amdgpu mesa lib32-mesa vulkan-radeon lib32-vulkan-radeon vulkan-icd-loader lib32-vulkan-icd-loader 
}


function nvidia() 
{
    yay -S nvidia nvidia-utils lib32-nvidia-utils nvidia-settings vulkan-icd-loader lib32-vulkan-icd-loader
}


function intel()
{
	yay -S xf86-video-intel mesa lib32-mesa vulkan-intel lib32-vulkan-intel vulkan-icd-loader lib32-vulkan-icd-loader 
}


function vm()
{
	echo " "
}


function gpu_drv_pkgs()
{
	if [ $gpu_vendor == "amd" ]; 
		then
		amd
		cd $tmp/ais && sudo cp 20-amdgpu.conf /etc/X11/xorg.conf.d/  

	elif [ $gpu_vendor == "nvidia" ];
		then	
		nvidia	
	
	elif [ $gpu_vendor == "intel" ];
		then	
		intel
		cd $tmp/ais && sudo cp 20-intel.conf /etc/X11/xorg.conf.d/ 
	
	elif [ $gpu_vendor == "vm" ];
		then	
		vm	

	else
		echo "Vendor not supported!"
		echo " "
		echo "Insert GPU vendor (amd, nvidia, intel, vm)"
		read gpu_vendor
		gpu_drv_pkgs
	fi
}

function laptop_setup()
{ 
	if [ $laptop == "y" ]; 
		then
		yay -S laptop-mode-tools acpi acpid
		cd $tmp/ais && sudo cp 40-libinput.conf /etc/X11/xorg.conf.d/ 
		sudo systemctl enable acpid.service
		sudo systemctl enable laptop-mode.service
	
	elif [ $laptop == "n" ];
		then
		echo " "

	else
		echo " "
		echo "No such option!"
		echo " "
		echo "Is your computer a laptop? (y/n)"
		read laptop
		laptop_setup
	fi
}


main

echo " "
echo "Insert GPU vendor (amd, nvidia, intel, vm)"
read gpu_vendor
echo " "
echo "Is your computer a laptop? (y/n)"
read laptop

gpu_drv_pkgs
laptop_setup

mv ~/config.sh $tmp
mv ~/config-gnome.sh $tmp

sed -i 's/PS1=/#PS1=/g' /home/$user/.bashrc
echo "PS1='(\[\033[1;32m\]\u@\h\[\033[1;34m\] \W\[\033[0m\])\$ '" >> /home/$user/.bashrc


#Configuration finished


systemctl enable --now gdm

