#!/bin/sh
echo " "
echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo "++++++++++++++++++++++++++++++++| Arch Linux installation |++++++++++++++++++++++++++++++++++"
echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo " "
echo "              To check your disk's name stop this script (ctrl+c), then enter:"
echo " "
echo "                                          lsblk "
echo " "
echo "                To erase disk before installation run the following command:"
echo " "
echo "                     dd if=/dev/zero of=/dev/sdx bs=4M status=progress"
echo " "
echo " "
echo "Insert disk's name (e.g. sdx, nvme0nx, vdx)"
read disk
if [ $disk == "sda" ] || [ $disk == "vda" ];
then
x1="1"
x2="2"
x3="3"
else
x1="p1"
x2="p2"
x3="p3"
fi
disk1="$disk$x1"
disk2="$disk$x2"
disk3="$disk$x3"
clear
echo " "
echo " "
echo " !WARNING! !WARNING! !WARNING! !WARNING! !WARNING! !WARNING! !WARNING! !WARNING! !WARNING!"
echo " "
echo "                        Partition disk using the following sheme:"
echo " "
echo "-----------------------------------------------------------------------------------------"
echo "                                  DOS partition table "
echo "                               /dev/sdx1 = Linux [system]"
echo "                               /dev/sdx2 = Linux [boot] *flag"
echo "                               /dev/sdx4 = Linux swap"
echo "-----------------------------------------------------------------------------------------"
echo " "
echo " "
read -p "Press ENTER to begin installation"
cfdisk /dev/$disk
modprobe dm-crypt
modprobe dm-mod
cryptsetup luksFormat -v -s 512 -h sha512 /dev/$disk1
echo " "
echo "Open encrypted partition:"
cryptsetup luksOpen /dev/$disk1 luks
mkfs.btrfs -L ArchLinux /dev/mapper/luks
mount -o noatime,discard,ssd,defaults /dev/mapper/luks /mnt
btrfs subvolume create /mnt/@root
btrfs subvolume create /mnt/@home
umount /mnt
mount -o subvol=@root /dev/mapper/luks /mnt
mkdir /mnt/{home,var,swap}
mount -o subvol=@home /dev/mapper/luks /mnt/home
mkfs.ext4 /dev/$disk2
mkswap /dev/$disk3
mkdir -p /mnt/boot
mount /dev/$disk2 /mnt/boot
swapon /dev/$disk3
clear
pacstrap /mnt linux linux-firmware base base-devel btrfs-progs nano
genfstab -U /mnt >> /mnt/etc/fstab
arch-chroot /mnt ln -sf /usr/share/zoneinfo/Europe/Warsaw /etc/localtime
arch-chroot /mnt hwclock --systohc --utc
sed -i '177s/#//' /mnt/etc/locale.gen
sed -i '390s/#//' /mnt/etc/locale.gen
arch-chroot /mnt locale-gen
echo "LANG=pl_PL.UTF-8" > /mnt/etc/locale.conf
echo " "
echo "Set hostname:"
read host
echo "$host" > /mnt/etc/hostname
echo " "
echo "Set ROOT password:"
arch-chroot /mnt passwd
echo " "
echo "Set username:"
read user
arch-chroot /mnt useradd -m -G wheel -s /bin/bash $user
echo " "
echo "Set USER password:"
arch-chroot /mnt passwd $user
echo " "
sed -i '82s/# //' /mnt/etc/sudoers
sed -i '33s/#//' /mnt/etc/pacman.conf
sed -i '93s/#//' /mnt/etc/pacman.conf
sed -i '94s/#//' /mnt/etc/pacman.conf
arch-chroot /mnt pacman -Sy grub efibootmgr os-prober
sed -i '7s/=""/="cryptdevice=\/dev\/'$disk1':ArchLinux:allow-discards"/' /mnt/etc/default/grub
sed -i '4s/=5/=0/' /mnt/etc/default/grub
sed -i '52s/filesystems/encrypt filesystems/' /mnt/etc/mkinitcpio.conf
arch-chroot /mnt mkinitcpio -p linux
arch-chroot /mnt grub-install --target=i386-pc --bootloader-id=ArchLinux /dev/$disk
arch-chroot /mnt grub-mkconfig -o /boot/grub/grub.cfg
arch-chroot /mnt pacman -S baobab eog evince file-roller gdm gnome-backgrounds gnome-calculator gnome-calendar gnome-characters gnome-clocks gnome-control-center gnome-disk-utility gnome-documents gnome-getting-started-docs gnome-keyring gnome-logs gnome-menus gnome-photos gnome-remote-desktop gnome-screenshot gnome-session gnome-settings-daemon gnome-shell gnome-shell-extensions gnome-system-monitor gnome-terminal gnome-themes-extra gnome-user-docs gnome-user-share gnome-video-effects gnome-tweaks grilo-plugins gvfs gvfs-goa gvfs-google gvfs-gphoto2 gvfs-mtp gvfs-nfs gvfs-smb mutter nautilus networkmanager rygel simple-scan sushi totem tracker tracker-miners vino xdg-user-dirs-gtk yelp gedit android-udev android-tools modemmanager usb_modeswitch git vim firefox noto-fonts-emoji
echo " "
echo "Insert GPU vendor (eg. amd, intel, nvidia, vm)"
echo " "
read gpu_vendor
function amd()
{
    	arch-chroot /mnt pacman -S xf86-video-amdgpu mesa lib32-mesa vulkan-radeon lib32-vulkan-radeon vulkan-icd-loader lib32-vulkan-icd-loader 
}

function intel()
{
    	arch-chroot /mnt pacman -S xf86-video-intel mesa lib32-mesa vulkan-intel lib32-vulkan-intel vulkan-icd-loader lib32-vulkan-icd-loader
}

function nvidia() 
{
    	arch-chroot /mnt pacman -S mesa mesa-libgl xf86-video-nouveau
		# proprietary drivers:  nvidia nvidia-utils lib32-nvidia-utils nvidia-settings vulkan-icd-loader lib32-vulkan-icd-loader
}

function vm() 
{
    	echo " "
		echo "nothing to do!"
		echo " "
}

function packages()
{
	if [ $gpu_vendor == "amd" ]; 
		then
		amd  
	
	elif [ $gpu_vendor == "intel" ];
		then	
		intel
	
	elif [ $gpu_vendor == "nvidia" ];
		then	
		nvidia

	elif [ $gpu_vendor == "vm" ];
		then	
		vm	
		
	else
		echo "Vendor not supported!"
		echo " "
		echo "Insert GPU vendor (eg. amd, intel, nvidia, vm)"
		read gpu_vendor
		packages
	fi
}
packages

arch-chroot /mnt systemctl enable systemd-networkd
arch-chroot /mnt systemctl enable systemd-resolved
arch-chroot /mnt systemctl enable NetworkManager
arch-chroot /mnt systemctl enable ModemManager
arch-chroot /mnt systemctl enable gdm
echo " "
echo "Installation finished!"
echo " "
read -p "Press ENTER to reboot"
umount -R /mnt
reboot
