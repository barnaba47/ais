#!/bin/sh

user=$(whoami)
mkdir /home/$user/.tmp
tmp="/home/"$user"/.tmp"
nmtui
clear
echo "Enable NTP"
echo " "
sudo timedatectl set-ntp true
 

function main()
{
	sudo rm -f /etc/fstab
	sudo mv /etc/fstab.b /etc/fstab
	sudo snapper -c root create-config /


	cd $tmp && git clone https://aur.archlinux.org/yay.git && cd yay && makepkg -si

	yay -S archlinux-keyring
	yay -S xorg xorg-xinit xdotool mingetty bspwm sxhkd rofi dunst picom alsa-utils pulseaudio pulseaudio-alsa xfce4-terminal thunar vim gsimplecal file-roller lxappearance lxrandr feh gst-libav papirus-icon-theme xss-lock android-udev android-tools firefox gthumb xclip trayer bash-completion gvfs-mtp reflector imagemagick bc noto-fonts-emoji gnu-free-fonts ttf-roboto nerd-fonts-roboto-mono ttf-hack

	yay -S ponymix-git lemonbar-xft-git betterlockscreen-git papirus-folders-git #i3lock-color-git

	cd $tmp && git clone https://github.com/baskerville/xdo.git && cd xdo && sudo make && sudo make install 
	cd $tmp  && git clone https://github.com/baskerville/sutils.git && cd sutils && sudo make && sudo make install
	cd $tmp && git clone https://github.com/baskerville/xtitle.git && cd xtitle && sudo make && sudo make install 
	cd $tmp && git clone https://gitlab.com/barnaba47/ais && cd ais && tar xf bspwm.tar.xz && cp -r $tmp/ais/home/. /home/$user
	
	cd /home/$user/.config/panel && chmod +x panel panel_bar /home/$user/.config/bspwm/delay
	sudo sed -i -e "\$aexport PANEL_FIFO="'"/tmp/panel-fifo"'" " /etc/profile
	sudo sed -i -e "\$aexport PATH=\$PATH:/home/$user/.config/panel" /etc/profile
	
	sudo mkdir -p /etc/X11/xorg.conf.d/ && cd $tmp/ais && sudo cp 10-monitor.conf /etc/X11/xorg.conf.d/ 
	
	#sudo mkdir -p /etc/systemd/journald.conf.d/ && cd /home/$user/Pobrane/ais && sudo cp usbstick.conf /etc/systemd/journald.conf.d/
	
	sudo cp $tmp/ais/audio_ctrl /bin && sudo chmod +x /bin/audio_ctrl
	
	sudo mv /bin/betterlockscreen /bin/betterlockscreen.b && rm -rf /bin/betterlockscreen
	sudo cp $tmp/ais/betterlockscreen /bin 
	
	sudo localectl --no-convert set-x11-keymap pl
	sudo localectl set-keymap --no-convert pl
	echo [Service] >> override.conf
	echo ExecStart= >> override.conf
	echo ExecStart=-/usr/bin/agetty --autologin $user --noclear \%I \$TERM >> override.conf 
	sudo chown root override.conf
	sudo chmod 644 override.conf
	sudo mkdir /etc/systemd/system/getty@tty1.service.d
	sudo mv override.conf /etc/systemd/system/getty@tty1.service.d/
	
	sudo cp -r $tmp/ais/logind.conf /etc/systemd/

	papirus-folders -C adwaita --theme Papirus-Dark
	
}


function amd()
{
	yay -S xf86-video-amdgpu mesa lib32-mesa vulkan-radeon lib32-vulkan-radeon vulkan-icd-loader lib32-vulkan-icd-loader 
}


function nvidia() 
{
    yay -S nvidia nvidia-utils lib32-nvidia-utils nvidia-settings vulkan-icd-loader lib32-vulkan-icd-loader
}


function intel()
{
	yay -S xf86-video-intel mesa lib32-mesa vulkan-intel lib32-vulkan-intel vulkan-icd-loader lib32-vulkan-icd-loader 
}


function vm()
{
	echo " "
}


function gpu_drv_pkgs()
{
	if [ $gpu_vendor == "amd" ]; 
		then
		amd
		cd $tmp/ais && sudo cp 20-amdgpu.conf /etc/X11/xorg.conf.d/  

	elif [ $gpu_vendor == "nvidia" ];
		then	
		nvidia	
	
	elif [ $gpu_vendor == "intel" ];
		then	
		intel
		cd $tmp/ais && sudo cp 20-intel.conf /etc/X11/xorg.conf.d/ 
	
	elif [ $gpu_vendor == "vm" ];
		then	
		vm	

	else
		echo "Vendor not supported!"
		echo " "
		echo "Insert GPU vendor (amd, nvidia, intel, vm)"
		read gpu_vendor
		gpu_drv_pkgs
	fi
}

function laptop_setup()
{ 
	if [ $laptop == "y" ]; 
		then
		yay -S laptop-mode-tools acpi acpid
		cd $tmp/ais && sudo cp 40-libinput.conf /etc/X11/xorg.conf.d/ 
		sudo systemctl enable acpid.service
		sudo systemctl enable laptop-mode.service
	
	elif [ $laptop == "n" ];
		then
		echo " "

	else
		echo " "
		echo "No such option!"
		echo " "
		echo "Is your computer a laptop? (y/n)"
		read laptop
		laptop_setup
	fi
}


main

echo " "
echo "Insert GPU vendor (amd, nvidia, intel, vm)"
read gpu_vendor
echo " "
echo "Is your computer a laptop? (y/n)"
read laptop

gpu_drv_pkgs
laptop_setup


mv ~/config.sh $tmp

sed -i 's/PS1=/#PS1=/g' /home/$user/.bashrc
echo "PS1='(\[\033[1;32m\]\u@\h\[\033[1;34m\] \W\[\033[0m\])\$ '" >> /home/$user/.bashrc

echo " "
echo "Configuration finished!"
echo " "
echo " "
echo "Press Enter to reboot"
read -p " "
reboot

