#!/bin/sh
echo " "
echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo "++++++++++++++++++++++++++++++++++| Arch Linux mounter |+++++++++++++++++++++++++++++++++++++"
echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo " "
echo "              To check your disk's name stop this script (ctrl+c), then enter:"
echo " "
echo "                                          lsblk "
echo " "
echo " "
echo "Insert disk's name (e.g. sdx, nvme0nx, vda)"
read disk
if [ $disk == "sda" ] || [ $disk == "vda" ];
then
x1="1"
x2="2"
x3="3"
x4="4"
else
x1="p1"
x2="p2"
x3="p3"
x4="p4"
fi
disk1="$disk$x1"
disk2="$disk$x2"
disk3="$disk$x3"
disk4="$disk$x4"
clear
echo " "
echo "Open encrypted partition:"
cryptsetup luksOpen /dev/$disk4 luks
mount -o subvol=@root /dev/mapper/luks /mnt
mount -o subvol=@home /dev/mapper/luks /mnt/home
mount /dev/$disk2 /mnt/boot
mount /dev/$disk1 /mnt/boot/efi

#cryptsetup open /dev/sda1 vg0
#mount /dev/mapper/vg0-data /mnt/data

clear
echo " "
echo "Mounted!"
echo " "

