#!/bin/bash

# Color definitions

main=$(zenity --info --text="Pick main accent color." && zenity --color-selection --show-palette)
lighter=$(zenity --info --text="Pick lighter accent color." && zenity --color-selection --show-palette)
darker=$(zenity --info --text="Pick darker accent color." && zenity --color-selection --show-palette)
text=$(zenity --info --text="Pick acent text color." && zenity --color-selection --show-palette)

hypr_upper=$main
hypr_lower=$darker

gtk_acc=$lighter
gtk_acc_bg=$main
gtk_acc_fg=$text

wofi_selected=$main

# File locations

gtk3=$HOME/.config/gtk-3.0/gtk.css
gtk4=$HOME/.config/gtk-4.0/gtk.css
hyprland=$HOME/.config/hypr/hyprland.conf
hyprlock=$HOME/.config/hypr/hyprlock.conf
wofi=$HOME/.config/wofi/style.css


# Swap values
sed -i "/    col.active_border/c\    col.active_border = $hypr_upper $hypr_lower 90deg" $hyprland
sed -i "/    col.inactive_border/c\    col.inactive_border = $hypr_lower" $hyprland

sed -i "/    outer_color =/c\    outer_color = $hypr_upper" $hyprlock

sed -i "/@define-color accent_color/c\@define-color accent_color $gtk_acc;" $gtk3
sed -i "/@define-color accent_bg_color/c\@define-color accent_bg_color $gtk_acc_bg;" $gtk3
sed -i "/@define-color accent_fg_color/c\@define-color accent_fg_color $gtk_acc_fg;" $gtk3

sed -i "/@define-color accent_color/c\@define-color accent_color $gtk_acc;" $gtk4
sed -i "/@define-color accent_bg_color/c\@define-color accent_bg_color $gtk_acc_bg;" $gtk4
sed -i "/@define-color accent_fg_color/c\@define-color accent_fg_color $gtk_acc_fg;" $gtk4

sed -i "/#entry:selected {/{n;s/^    background-color.*/    background-color: $main;/}" $wofi

hyprctl reload
