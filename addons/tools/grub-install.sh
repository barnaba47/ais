#!/bin/sh
echo " "
echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo "+++++++++++++++++++++++++++++++++| Arch Linux GRUB installer |+++++++++++++++++++++++++++++++"
echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo " "
echo "                              Mount your filesystems first !"
echo " "
echo "              To check your disk's name stop this script (ctrl+c), then enter:"
echo " "
echo "                                          lsblk "
echo " "
echo " "
echo "Insert disk's name (e.g. sdx, nvme0nx, vda)"
read disk
if [ $disk == "sda" ] || [ $disk == "vda" ];
then
x1="1"
x2="2"
x3="3"
x4="4"
else
x1="p1"
x2="p2"
x3="p3"
x4="p4"
fi
disk1="$disk$x1"
disk2="$disk$x2"
disk3="$disk$x3"
disk4="$disk$x4"
clear
arch-chroot /mnt pacman -Sy grub efibootmgr os-prober
echo " "
echo "Before installing check '/etc/default/grub' and '/etc/mkinitcpio.conf'"
#sed -i '7s/=""/="cryptdevice=\/dev\/'$disk4':ArchLinux:allow-discards"/' /mnt/etc/default/grub
#sed -i '4s/=5/=0/' /mnt/etc/default/grub
#sed -i '52s/filesystems/encrypt filesystems/' /mnt/etc/mkinitcpio.conf
echo " "
echo -p "Press Enter to install GRUB"
echo " "
arch-chroot /mnt mkinitcpio -p linux
arch-chroot /mnt grub-install --efi-directory=/boot/efi /dev/$disk2 --bootloader-id=ArchLinux
arch-chroot /mnt grub-mkconfig -o /boot/grub/grub.cfg
arch-chroot /mnt grub-mkconfig -o /boot/efi/EFI/ArchLinux/grub.cfg
echo " "
echo "Done!"
echo " "
