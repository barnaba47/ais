#
# ~/.bash_profile
#

[[ -f ~/.bashrc ]] && . ~/.bashrc
export PATH="$PATH:$HOME/.scripts"
export WLR_NO_HARDWARE_CURSORS=0
export MOZ_ENABLE_WAYLAND=1 
export AMD_DEBUG=lowlatencyenc

# sudo usbreset USB2.0-BT

case "$(tty)" in
    /dev/tty1)
        sh $HOME/.scripts/gamescope-session
        ;;
    /dev/tty2)
        Hyprland
        ;;
    *)
        ;;
esac