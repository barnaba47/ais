#!/bin/bash

# Variables
MONITOR=DP-3
SCREEN_WIDTH=2560
SCREEN_HEIGHT=1440
REFRESH_RATE=165

sudo echo " "

# Install required packages
sudo pacman -S util-linux steam gamescope mangohud lib32-mangohud

# Create autologin service
sudo mkdir -p /etc/systemd/system/getty@tty4.service.d

cat <<EOF > /tmp/autologin.conf
[Service]
ExecStart=
ExecStart=-/sbin/agetty -o '-p -f -- \\u' --noclear --autologin $USER %I \$TERM
EOF

sudo mv /tmp/autologin.conf /etc/systemd/system/getty@tty4.service.d/

# Create .bash_profile
cat <<EOF > /tmp/.bash_profile_gamemode
export SDL_VIDEO_MINIMIZE_ON_FOCUS_LOSS=0
export STEAM_MANGOAPP_PRESETS_SUPPORTED=1
export STEAM_USE_MANGOAPP=1
export MANGOHUD_CONFIGFILE=/home/$USER/.config/MangoHud/MangoHud.conf
export STEAM_USE_DYNAMIC_VRS=1
export RADV_FORCE_VRS_CONFIG_FILE=/home/$USER/.config/RADV/radv_vrs
export STEAM_GAMESCOPE_HDR_SUPPORTED=1
export WINEDLLOVERRIDES=dxgi=n
export STEAM_ENABLE_VOLUME_HANDLER=1
export SRT_URLOPEN_PREFER_STEAM=1
export STEAM_DISABLE_AUDIO_DEVICE_SWITCHING=1
export STEAM_MULTIPLE_XWAYLANDS=1
export STEAM_GAMESCOPE_DYNAMIC_FPSLIMITER=1
export STEAM_GAMESCOPE_HAS_TEARING_SUPPORT=1
# export STEAM_GAMESCOPE_NIS_SUPPORTED=1
export STEAM_GAMESCOPE_TEARING_SUPPORTED=1
export STEAM_GAMESCOPE_VRR_SUPPORTED=1
export STEAM_GAMESCOPE_DYNAMIC_REFRESH_IN_STEAM_SUPPORTED=0
export vk_xwayland_wait_ready=false
export STEAM_DISABLE_MANGOAPP_ATOM_WORKAROUND=1
export STEAM_MANGOAPP_HORIZONTAL_SUPPORTED=1
export STEAM_GAMESCOPE_FANCY_SCALING_SUPPORT=1
export STEAM_GAMESCOPE_COLOR_MANAGED=1
export STEAM_GAMESCOPE_VIRTUAL_WHITE=1
export GAMESCOPE_DISABLE_ASYNC_FLIPS=1
export STEAM_GAMESCOPE_FORCE_HDR_DEFAULT=1
export STEAM_GAMESCOPE_FORCE_OUTPUT_TO_HDR10PQ_DEFAULT=1
export STEAM_DISPLAY_REFRESH_LIMITS=48,165
export VKD3D_SWAPCHAIN_LATENCY_FRAMES=3
export GAMESCOPE_NV12_COLORSPACE=k_EStreamColorspace_BT601
export ENABLE_GAMESCOPE_WSI=1
export mesa_glthread=true

# To play nice with the short term callback-based limiter for now
export GAMESCOPE_LIMITER_FILE=$(mktemp /tmp/gamescope-limiter.XXXXXXXX)

# Plop GAMESCOPE_MODE_SAVE_FILE into $XDG_CONFIG_HOME (defaults to ~/.config).
export GAMESCOPE_MODE_SAVE_FILE="${XDG_CONFIG_HOME:-$HOME/.config}/gamescope/modes.cfg"
export GAMESCOPE_PATCHED_EDID_FILE="${XDG_CONFIG_HOME:-$HOME/.config}/gamescope/edid.bin"

# Make path to gamescope mode save file.
mkdir -p "$(dirname "$GAMESCOPE_MODE_SAVE_FILE")"
touch "$GAMESCOPE_MODE_SAVE_FILE"
#echo "Making Gamescope Mode Save file at \"$GAMESCOPE_MODE_SAVE_FILE\""

# Make path to Gamescope edid patched file.
mkdir -p "$(dirname "$GAMESCOPE_PATCHED_EDID_FILE")"
touch "$GAMESCOPE_PATCHED_EDID_FILE"

# export GAMESCOPE_OVERRIDE_REFRESH_RATE=60,165
# export STEAM_GAMESCOPE_NIS_SUPPORTED=1

ulimit -n 524288

gamescope --generate-drm-mode fixed --hdr-enabled --adaptive-sync --xwayland-count 2 -W $SCREEN_WIDTH -H $SCREEN_HEIGHT -r $REFRESH_RATE --hide-cursor-delay 3000 -O $MONITOR \
-- steam -gamepadui -steamos3 -steampal -steamdeck -pipewire-dmabuf
EOF

sudo mkdir -p /home/$USER/
sudo mv /tmp/.bash_profile_gamemode /home/$USER/

# Create MangoHud config
cat <<EOF > /tmp/MangoHud.conf
### pre defined presets
# -1 = default
#  0 = no display
#  1 = fps only
#  2 = horizontal view
#  3 = extended
#  4 = high detailed information
#preset=2

no_display
fps_limit=0,144,120,100,90,80,70,60,50,40,30
EOF
mkdir -p /home/$USER/.config/MangoHud/
mv /tmp/MangoHud.conf /home/$USER/.config/MangoHud/

# Set RADV vars
cat <<EOF > /tmp/radv_vrs
1x1
EOF

mkdir -p /home/$USER/.config/RADV/
mv /tmp/radv_vrs /home/$USER/.config/RADV/