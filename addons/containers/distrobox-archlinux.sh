#!/bin/bash

sudo echo " "

clear

echo " "
echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo "++++++++++++++++++++++++++| Arch Linux container image creation |++++++++++++++++++++++++++++"
echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo " "
echo " "
echo " "
read -p "Press ENTER to install dependencies"
echo " "

sudo pacman -Syu podman distrobox arch-install-scripts

echo " "
echo " "
read -p "Press ENTER to configure podman"
echo " "

sudo systemctl enable --now podman

sudo touch /etc/subuid /etc/subgid

sudo usermod --add-subuid 100000-165535 --add-subgid 100000-165535 $USER

echo " "
echo " "
read -p "Press ENTER to download rootfs (pacstrap)"
echo " "

mkdir /tmp/archlinux-rootfs

sudo pacstrap -c /tmp/archlinux-rootfs base base-devel

echo " "
echo " "
read -p "Press ENTER to configure system (arch-chroot)"
echo " "

sudo arch-chroot /tmp/archlinux-rootfs ln -sf /usr/share/zoneinfo/Europe/Warsaw /etc/localtime

sudo arch-chroot /tmp/archlinux-rootfs hwclock --systohc --utc

sudo sed -i '/en_US.UTF-8 UTF-8/s/^#//g' /tmp/archlinux-rootfs/etc/locale.gen

sudo arch-chroot /tmp/archlinux-rootfs locale-gen

echo "LANG=en_US.UTF-8" | sudo tee -a  /tmp/archlinux-rootfs/etc/locale.conf

echo " "

echo "Set username:"

read user

sudo arch-chroot /tmp/archlinux-rootfs useradd -m -G wheel -s /bin/bash $user
echo " "

echo "Set USER password:"

sudo arch-chroot /tmp/archlinux-rootfs passwd $user

echo " "

sudo arch-chroot /tmp/archlinux-rootfs mkdir /home/$user/.tmp

sudo arch-chroot /tmp/archlinux-rootfs chown $user:$user /home/$user/.tmp

sudo sed -i '/%wheel ALL=(ALL:ALL) ALL/s/^#//g' /tmp/archlinux-rootfs/etc/sudoers

sudo sed -i '/#Color/s/^#//' /tmp/archlinux-rootfs/etc/pacman.conf

sudo sed -i '/#\[multilib\]/s/^#//' /tmp/archlinux-rootfs/etc/pacman.conf

sudo sudo sed -i '/\[multilib\]/{n;s/^#//}' /tmp/archlinux-rootfs/etc/pacman.conf

echo " "
echo " "
read -p "Press ENTER to create image archive"
echo " "

sudo tar --create --verbose --gzip --file=/tmp/archlinux-rootfs.tar.gz --directory=/tmp/archlinux-rootfs .
echo " "
echo " "
echo "Custom Arch Linux image has been created!"

echo " "
echo " "
read -p "Press ENTER to import image"
echo " "

cat /tmp/archlinux-rootfs.tar.gz | podman import - localhost/archlinux-custom:latest

echo " "
echo " "
echo "Custom Arch Linux image has been imported!"
