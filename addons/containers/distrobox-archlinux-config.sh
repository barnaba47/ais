#!/bin/bash

rm -rf /tmp/*

sudo pacman -Su base-devel fakeroot archlinux-keyring git papirus-icon-theme file-roller ttf-hack cantarell-fonts kitty ranger lxsession-gtk3 noto-fonts-emoji gamescope xdg-utils xdg-user-dirs xdg-desktop-portal-hyprland neovim wl-clipboard imv wofi otf-font-awesome libadwaita ttf-roboto

usr=$USER

sed -i 's/PS1=/#PS1=/g' ~/.bashrc
echo "PS1='(\[\033[1;33m\]\u@\h\[\033[1;34m\] \W\[\033[0m\])\$ '" >> ~/.bashrc
echo 'export EDITOR="nvim"' >> ~/.bashrc
echo 'export VISUAL="nvim"' >> ~/.bashrc

xdg-user-dirs-update --force


cd /tmp && git clone https://gitlab.com/barnaba47/ais

cd /tmp/ais && tar -xf hyprland.tar.xz

rsync -av /tmp/ais/home/ ~/


cd /tmp && git clone https://aur.archlinux.org/adw-gtk3.git && cd adw-gtk3 && makepkg -si
cd /tmp && git clone https://aur.archlinux.org/adw-gtk-theme.git && cd adw-gtk-theme && makepkg -si
dbus-launch gsettings set org.gnome.desktop.interface gtk-theme "Adw-dark"
dbus-launch gsettings set org.gnome.desktop.interface color-scheme "prefer-dark"
dbus-launch gsettings set org.gnome.desktop.interface icon-theme "Papirus-Dark"
dbus-launch gsettings set org.gnome.desktop.interface cursor-theme "DMZ-White"
dbus-launch gsettings set org.gnome.desktop.interface font-name "Roboto 11"
dbus-launch gsettings set org.gnome.desktop.interface monospace-font-name "Hack 11"
dbus-launch gsettings set org.gnome.desktop.interface font-hinting "slight"
dbus-launch gsettings set org.gnome.desktop.interface font-antialiasing "rgba"
