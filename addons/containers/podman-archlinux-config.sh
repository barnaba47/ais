#!/bin/bash

sudo pacman -Su git rsync archlinux-keyring thunar pipewire pipewire-alsa pipewire-pulse pipewire-jack wireplumber \
	            pavucontrol papirus-icon-theme file-roller ttf-hack cantarell-fonts kitty firefox gsimplecal \
	      	    lxsession-gtk3 dunst noto-fonts-emoji bluez bluez-utils blueman gamescope xdg-utils xdg-user-dirs \
	      	    vim imv acpid wofi otf-font-awesome libadwaita ttf-roboto cpupower net-tools libnotify \
		        xorg-xsetroot playerctl gvfs gvfs-mtp gnome-text-editor ttf-hack-nerd adw-gtk-theme

sed -i 's/PS1=/#PS1=/g' ~/.bashrc
echo "PS1='(\[\033[1;33m\]\u@\h\[\033[1;34m\] \W\[\033[0m\])\$ '" >> ~/.bashrc

xdg-user-dirs-update --force


cd /tmp && git clone https://gitlab.com/barnaba47/ais

cd /tmp/ais && tar -xf hyprland.tar.xz

rsync -av /tmp/ais/home/ ~/


dbus-launch gsettings set org.gnome.desktop.interface gtk-theme "adw-gtk3-dark"
dbus-launch gsettings set org.gnome.desktop.interface color-scheme "prefer-dark"
dbus-launch gsettings set org.gnome.desktop.interface icon-theme "Papirus-Dark"
dbus-launch gsettings set org.gnome.desktop.interface cursor-theme "DMZ-White"
dbus-launch gsettings set org.gnome.desktop.interface font-name "Roboto 11"
dbus-launch gsettings set org.gnome.desktop.interface monospace-font-name "Hack 11"
dbus-launch gsettings set org.gnome.desktop.interface font-hinting "slight"
dbus-launch gsettings set org.gnome.desktop.interface font-antialiasing "rgba"