#!/bin/bash

set -x

# Unload VFIO-PCI Kernel Driver
modprobe -r vfio-pci
modprobe -r vfio_iommu_type1
modprobe -r vfio

# Re-Bind GPU to AMD Driver
virsh nodedev-reattach pci_0000_09_00_1 
virsh nodedev-reattach pci_0000_09_00_0

# Rebind VT consoles
echo "efi-framebuffer.0" > /sys/bus/platform/drivers/efi-framebuffer/bind
echo 1 > /sys/class/vtconsole/vtcon0/bind


#Load amd driver
modprobe amdgpu

ttyecho -n /dev/tty2 Hyprland

