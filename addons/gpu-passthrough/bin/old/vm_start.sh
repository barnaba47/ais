#!/bin/bash

# Helpful to read output when debugging
set -x
 
mv /home/szymon/.bash_profile /home/szymon/.bash_profile.b
kill -9 $(pgrep panel) 
kill -9 $(pgrep bspwm)
sleep 2

# Unbind VTconsoles
echo 0 > /sys/class/vtconsole/vtcon0/bind 
echo efi framebuffer.0 > /sys/bus/platform/drivers/efi-framebuffer/unbind
sleeep 2 

# Unload AMD drivers
modprobe -r amdgpu

# Unbind the GPU from display driver
virsh nodedev-detach pci_0000_01_00_0 
virsh nodedev-detach pci_0000_01_00_1

# Load VFIO kernel module
modprobe vfio-pci

