#! /bin/sh

# sudo pacman -S lxd
# sudo systemctl enable --now lxd
# sudo lxd init
# sudo usermod -a -G lxd $(whoami)
# relogin/reboot

lang="pl_PL.UTF-8 UTF-8"

lang2=$(echo "$lang" | cut -d ' ' -f 1)
keymap=$(echo "$lang" | cut -c 4-5 )
keymap=$(echo "$keymap" | awk '{print tolower($0)}')

echo "Enter container name:"

read cname

lxc launch images:archlinux/current/amd64 $cname -c security.privileged=true

echo "root:$UID:1" | sudo tee -a /etc/subuid /etc/subgid

lxc config set $cname raw.idmap "both $UID 1000"

lxc restart $cname

lxc config device add $cname mygpu gpu

lxc config device set $cname mygpu uid 1000

lxc config device set $cname mygpu gid 1000

lxc exec $cname -- sed -i '/%wheel ALL=(ALL:ALL) ALL/s/^#//g' /etc/sudoers

lxc exec $cname -- sed -i '33s/#//' /etc/pacman.conf

lxc exec $cname -- sed -i '93s/#//' /etc/pacman.conf

lxc exec $cname -- sed -i '94s/#//' /etc/pacman.conf

echo "Enter new username:"

read usr

lxc exec $cname -- useradd -m -G wheel $usr

lxc exec $cname -- passwd $usr

lxc exec $cname -- su - $usr << EOF
mkdir /home/$usr/.socket /home/$usr/.1000
EOF

lxc config device add $cname X0 disk path=/home/$usr/.socket/X0 source=/tmp/.X11-unix/X0

lxc config device add $cname u1000 disk path=/home/$usr/.1000 source=/run/user/1000

lxc config device add $cname Pulse proxy bind=container connect=tcp:127.0.0.1:4713 listen=tcp:127.0.0.1:4713

sudo sed -i '$ a load-module module-native-protocol-tcp auth-ip-acl=127.0.0.1' /etc/pulse/default.pa

pulseaudio -k

lxc exec $cname -- pacman -Syu base-devel fakeroot vim git gnome-console nautilus gnome-tweaks gnome-control-center pulseaudio pulseaudio-alsa bash-completion ttf-hack adobe-source-code-pro-fonts xdg-user-dirs xdg-utils papirus-icon-theme

lxc exec $cname -- mv /etc/locale.gen /etc/locale.gen.bak

lxc exec $cname -- bash -c "echo $lang | tee -a /etc/locale.gen"

lxc exec $cname -- mv /etc/locale.conf /etc/locale.conf.bak

lxc exec $cname -- bash -c 'echo LANG=\"'$lang2'\" | tee -a /etc/locale.conf'

lxc exec $cname -- locale-gen

lxc exec $cname -- localectl set-keymap --no-convert $keymap

lxc exec $cname -- localectl set-x11-keymap --no-convert $keymap

lxc exec $cname -- xdg-user-dirs-update --force

lxc exec $cname -- su - $usr << EOF
sed -i -e "$ a sh ~/.startup" /home/$usr/.bash_profile
echo "#!/bin/bash" >> .startup
echo "export DISPLAY=:0" >> .startup
echo "export PULSE_SERVER=tcp:127.0.0.1:4713" >> .startup
echo "export WAYLAND_DISPLAY=wayland-0" >> .startup
echo "export QT_QPA_PLATFORM=wayland" >> .startup
echo "bash" >> .startup
sed -i 's/PS1=/#PS1=/g' /home/$usr/.bashrc
echo "PS1='(\[\033[1;33m\]\u@\h\[\033[1;34m\] \W\[\033[0m\])\$ '" >> /home/$usr/.bashrc
EOF

lxc restart $cname

echo "#! /bin/sh" >> lc
echo " " >> lc
echo '# Xbox controller support requires "xow" or "xone" package on the host' >> lc
echo '# "lxc exec '$cname' -- reboot" is needed when X11 session is restarted!' >> lc
echo " " >> lc 
echo "xhost +local:" >> lc
echo "clear" >> lc
echo 'stat=$(lxc list | grep '$cname')' >> lc
echo 'running=$(lxc list | grep '$cname' | grep RUNNING)' >> lc
echo 'stopped=$(lxc list | grep '$cname' | grep STOPPED)' >> lc
echo 'xone=$(ls /dev/input | grep js0)' >> lc
echo "function launch {" >> lc
echo "lxc exec $cname -- bash  << EOF" >> lc
echo "touch /tmp/.X11-unix/X0" >> lc
echo "mount --bind /home/$usr/.socket/X0 /tmp/.X11-unix/X0" >> lc
echo "chmod -R 777 /dev/input" >> lc
echo "EOF" >> lc
echo "lxc exec $cname -- su - $usr << EOF" >> lc
echo "rofi -show run&" >> lc 
echo "pax11publish -e" >> lc
echo "EOF" >> lc
echo "}" >> lc
echo " " >> lc
echo 'if [[ $xone == "js0" ]]; then' >> lc
echo $'event=$(cat /proc/bus/input/devices | grep "js0" | grep -oP \'(?=event)[^ ]*\')' >> lc
echo "  lxc config device add $cname js0 unix-char path=/dev/input/js0" >> lc
echo "  lxc config device add $cname jsevent unix-char path=/dev/input/$event" >> lc
echo "else" >> lc
echo "  lxc config edit $cname < ~/.lxc/$cname/$cname.yaml" >> lc
echo "fi" >> lc
echo 'if [[ $stat == $running ]]; then' >> lc
echo "	launch" >> lc
echo 'elif [[ $stat == $stopped ]]; then' >> lc
echo '	lxc start '$cname'' >> lc
echo "	sleep 3" >> lc
echo "  launch"  >> lc
echo "else" >> lc
echo "	lxc_return=1" >> lc
echo "fi" >> lc
echo "lxc config edit $cname < ~/.lxc/$cname/$cname.yaml" >> lc

sudo mkdir -p /home/$USER/.lxc/$cname && sudo chmod -R 777 /home/$USER/.lxc  
sudo chmod +x lc && sudo mv lc /home/$USER/.lxc/$cname/$cname
sudo ln -s ~/.lxc/$cname/$cname /bin/$cname

lxc config show $cname >> /home/$USER/.lxc/$cname/$cname.yaml
