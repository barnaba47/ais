#!/bin/sh

lang="pl_PL.UTF-8 UTF-8"

lang2=$(echo "$lang" | cut -d ' ' -f 1)
keymap=$(echo "$lang" | cut -c 4-5 )
keymap=$(echo "$keymap" | awk '{print tolower($0)}')

user=$(whoami)
tmp="/tmp"
nmtui
clear
echo "Enable NTP"
echo " "
sudo timedatectl set-ntp true

function main()
{
	sudo rm -f /etc/fstab
	sudo mv /etc/fstab.b /etc/fstab

	# Configuring snapper
	sudo snapper -c root create-config /
	sudo snapper -c home create-config /home

	sudo snapper -c root create --description init
	sudo snapper -c home create --description init

	# Configuring locales
	sudo mv /etc/locale.gen /etc/locale.gen.bak
	echo $lang | sudo tee -a /etc/locale.gen
	sudo mv /etc/locale.conf /etc/locale.conf.bak
	echo LANG='"'$lang2'"' | sudo tee -a /etc/locale.conf
	sudo locale-gen
	sudo localectl set-keymap --no-convert $keymap
	sudo localectl set-x11-keymap --no-convert $keymap

	# Installing packages
	sudo pacman -S gnome-shell gdm xdg-utils xdg-user-dirs gnome-console gnome-text-editor gnome-connections gnome-calendar gnome-font-viewer gnome-system-monitor nautilus gnome-backgrounds gnome-tweaks gnome-control-center gnome-calculator firefox file-roller android-udev android-tools gnome-photos eog bash-completion gvfs-mtp reflector papirus-icon-theme noto-fonts-emoji ttf-hack acpi acpid vim man cpupower fprintd
	
	# Creating user's home subdirectories
	rm -rf ~/*
	export LANG=$lang2
	xdg-user-dirs-update --force

	# Enable acpid and fingerprint services
	cd $tmp/ais && sudo cp 40-libinput.conf /etc/X11/xorg.conf.d/ 
	sudo systemctl enable acpid.service

	sudo systemctl enable fprintd.service
}

function aur()
{
	cd $tmp && git clone https://aur.archlinux.org/adw-gtk3-git.git
	cd adw-gtk3-git
	makepkg -si

	cd $tmp && git clone https://aur.archlinux.org/libbacktrace-git.git
	cd libbacktrace-git
	makepkg -si

	cd $tmp && git clone https://aur.archlinux.org/text-engine-git.git
	cd text-engine-git
	makepkg -si

	cd $tmp && git clone https://aur.archlinux.org/extension-manager-git.git
	cd extension-manager-git
	makepkg -si

	cd $tmp && git clone https://aur.archlinux.org/papirus-folders-git.git
	cd papirus-folders-git
	makepkg -si
	papirus-folders -C adwaita --theme Papirus-Dark
}

function amd()
{
	sudo pacman -S xf86-video-amdgpu mesa lib32-mesa vulkan-radeon lib32-vulkan-radeon vulkan-icd-loader lib32-vulkan-icd-loader 
}


function nvidia() 
{
    sudo pacman -S nvidia nvidia-utils lib32-nvidia-utils nvidia-settings vulkan-icd-loader lib32-vulkan-icd-loader
}


function intel()
{
	sudo pacman -S xf86-video-intel mesa lib32-mesa vulkan-intel lib32-vulkan-intel vulkan-icd-loader lib32-vulkan-icd-loader 
}


function vm()
{
	echo " "
}

function gpu_drv_pkgs()
{
	if [ $gpu_vendor == "amd" ]; 
		then
		amd
		cd $tmp/ais && sudo cp 20-amdgpu.conf /etc/X11/xorg.conf.d/  

	elif [ $gpu_vendor == "nvidia" ];
		then	
		nvidia	
	
	elif [ $gpu_vendor == "intel" ];
		then	
		intel
		cd $tmp/ais && sudo cp 20-intel.conf /etc/X11/xorg.conf.d/ 
	
	elif [ $gpu_vendor == "vm" ];
		then	
		vm	

	else
		echo "Vendor not supported!"
		echo " "
		echo "Insert GPU vendor (amd, nvidia, intel, vm)"
		read gpu_vendor
		gpu_drv_pkgs
	fi
}

echo " "
echo "Installing and configuring main packages..."
echo " "
main

echo " "
echo "Insert GPU vendor (amd, nvidia, intel, vm)"
read gpu_vendor
echo " "
gpu_drv_pkgs

echo " "
echo "Installing and configuring AUR packages..."
echo " "
aur

mv ~/config.sh $tmp
mv ~/config-gnome.sh $tmp

# Cleaning up
sed -i 's/PS1=/#PS1=/g' /home/$user/.bashrc
echo "PS1='(\[\033[1;94m\]\u@\h\[\033[1;34m\] \W\[\033[0m\])\$ '" >> /home/$user/.bashrc

#Configuration finished

systemctl enable --now gdm